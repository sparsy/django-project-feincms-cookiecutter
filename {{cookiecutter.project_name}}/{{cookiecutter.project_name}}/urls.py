# -*- coding: utf-8 -*-

"""{{cookiecutter.project_title}} URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
{% if cookiecutter.redirect_root_url_to_admin == "y" -%}
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView
{%- endif %}


urlpatterns = [

    # ADMIN URL
    {% if cookiecutter.admindocs == "y" -%}
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    {%- endif %}
    url(r'^admin/', admin.site.urls),
    {% if cookiecutter.redirect_root_url_to_admin == "y" -%}
    url(r'^$', RedirectView.as_view(url=reverse_lazy('admin:index'))),
    {%- endif %}

    {% if cookiecutter.djangorestframework == "y" -%}
    # DRF API URL
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    {% if not cookiecutter.project_app_title == "None" -%}
    url(r'^api/{{cookiecutter.project_app_name}}/', include('apps.{{cookiecutter.project_app_name}}.api.urls', namespace='{{cookiecutter.project_app_name}}')),
    {% endif -%}
    {%- endif %}
]

if settings.DEBUG:
    # ON DEBUG

    # MEDIA
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
{% if cookiecutter.django_debug_toolbar == "y" %}
    # DEBUG TOOLBAR
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
{%- endif %}
