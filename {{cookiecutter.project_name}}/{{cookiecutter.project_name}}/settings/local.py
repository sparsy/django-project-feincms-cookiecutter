# -*- coding: utf-8 -*-

# Put here your local settings

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '!!SECRET_KEY!!'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

{% if cookiecutter.database_engine == "Postgres" %}
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{cookiecutter.db_name}}',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}
{% else %}
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': '{{cookiecutter.db_name}}.sqlite3',
    }
}
{% endif %}


{%- if cookiecutter.email_backend_smtp == "Empty" %}
EMAIL_HOST = ''
EMAIL_PORT = ''
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = True
{% elif cookiecutter.email_backend_smtp == "Gmail" %}
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = '587'
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = True
{% endif %}

# To send email to admins:
# from django.core.mail import mail_admins
# mail_admins('my subject', 'site is going down.')
ADMINS = [
    ('{{cookiecutter.author}}', '{{cookiecutter.email}}'),
]
